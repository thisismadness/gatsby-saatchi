import React from 'react';
import { StaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';
import tileStyles from '../components/tile.module.css';

//images 
import imageOverlay from  '../images/tiles/image-overlay.svg'
const Tile = (props) => (
  <div className="col-sm-6 col-md-4">
    <div className={tileStyles.tileWrapper}>
      <div className={tileStyles.tileHeader}>
        <img className={tileStyles.tileImage} src={props.imgSrc} />
        <img className={tileStyles.tileImageOverlay} src={imageOverlay} />
        <h3 className={tileStyles.tileImageText}>Lexus <br/>ES</h3>
      </div>
      
     
      <div className={tileStyles.tileContent}>
        <h3 className={tileStyles.tileTitle}>Expect exhilaration worthy of the bold.</h3>
        <a className={tileStyles.tileLink} href="#">See GS features</a>
        <a href="#" className={tileStyles.tileButton}>Select Lexus GS</a>
      </div>
    </div>
  </div>
)
export default Tile
