import React from 'react';
import Layout from '../components/layout';
//import Image from '../components/image';

//Import react components
import Tile from '../components/tile';

//Import page css modules
import indexStyles from '../components/index.module.css';

//Import images
import lexusToYouWhite from '../images/LexusToYouWhite.svg';
import select from '../images/Select.svg';
import notification from '../images/Notification.svg';
import location from '../images/Location.svg';
import divider from '../images/divider.svg';
import stepOne from '../images/step-1.svg';
import scroll from '../images/scroll.svg';
import backToTop from '../images/back-to-top.svg';

import image1 from '../images/tiles/tile-1.png';
import image2 from '../images/tiles/tile-2.png';
import image3 from '../images/tiles/tile-3.png';
import image4 from '../images/tiles/tile-4.png';
import image5 from '../images/tiles/tile-5.png';
import image6 from '../images/tiles/tile-6.png';


const IndexPage = () => (
  <Layout>
    <div className={indexStyles.navbar}>
      <img className={indexStyles.logo} src={lexusToYouWhite} alt="lexus To You" />
    </div>
    <div className={indexStyles.titleWrapper}>
        <h1 className={indexStyles.title}>Test-Drives now come to you</h1>
    </div>
    <p className={indexStyles.lead}>How it works</p>
    <div className="row">
      <div className="col-md-4">
        <div className={indexStyles.carStepsWrapper}>
          <p>
            <img className={indexStyles.carStepsIcon} src={select}  />
             <span className={indexStyles.carStepsText}>Select your car</span>
            </p>
          </div>
      
      </div>

      <div className="col-md-4">
        <div className={indexStyles.carStepsWrapper}>
          <p> 
            <img className={indexStyles.carStepsIcon} src={location}  />
            <span className={indexStyles.carStepsText}>Name the time and place</span>
          </p>
        </div>
      </div>

      <div className="col-md-4">
        <div className={indexStyles.carStepsWrapper}>
          <p> 
            <img className={indexStyles.carStepsIcon} src={notification}  />
            <span className={indexStyles.carStepsText}>Get notified on your cars arrival</span>
          </p>
        </div>
      </div>
    </div>

       <div className="row">
        <div className="col-md-12">
          <div className={indexStyles.divider}>
          </div>
          {/* <img src={divider} />*/}
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          <p className={indexStyles.stepDescription}>Step 1. Select your car</p>
          <div className="row">
            <div className="col-md-offset-3 col-md-6">
              <img src={stepOne} className={indexStyles.stepOne} />
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          <img className={indexStyles.stepDescription} src={scroll} alt="scroll" />
        </div>
      </div>

   <div className="row">
      <Tile imgSrc={image1} />
      <Tile imgSrc={image2} />
      <Tile imgSrc={image3} />
      <Tile imgSrc={image4} />
      <Tile imgSrc={image5} />
      <Tile imgSrc={image6} />
    </div>
    <div className="row">
      <div className="col-md-12">
        <p className={indexStyles.backToTop}>
          <a  href="#">
            <img src={backToTop}/><br/>
            Back to top
          </a>
        </p>
      </div>
    </div>
  </Layout>
)

export default IndexPage
